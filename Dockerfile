FROM openjdk:8-jdk-alpine
VOLUME [ "/tmp" ]
ARG JAVA_OPTS
ENV JAVA_OPTS=${JAVA_OPTS}
ADD target/*.jar gestuser.jar
EXPOSE 8019
ENTRYPOINT exec java ${JAVA_OPTS} -jar gestuser.jar
