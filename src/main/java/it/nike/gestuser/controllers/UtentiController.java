package it.nike.gestuser.controllers;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import it.nike.gestuser.models.Utenti;
import it.nike.gestuser.services.UtentiService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
 
 
@RestController
@RequestMapping(value = "/utenti")
public class UtentiController
{
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private UtentiService utentiService;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Value("${java}")
	private String value;

	@GetMapping(value = "/print")
	public void print()
	{
		System.out.println(value);

	}


	@GetMapping(value = "/cerca/tutti")
	public List<Utenti> getAllUser()
	{
		logger.info("Otteniamo tutti gli utenti");

		return utentiService.getAll();
	}
	
	@GetMapping(value = "/cerca/userid/{userId}")
	public Utenti getUtente(@PathVariable("userId") String userId) 
	{
		logger.info("Otteniamo l'utente " + userId);
		
		Utenti retVal = utentiService.getUtente(userId);
		
		return retVal;
	}
	
	@PostMapping(value = "/inserisci")
	public ResponseEntity<Utenti> addNewUser(@Valid @RequestBody Utenti utente, 
		BindingResult bindingResult)
	{
		logger.info("Inserimento Nuovo Utente");

		if (bindingResult.hasErrors())
		{
			String MsgErr = "Errore Validazione Password";
			
			logger.warn(MsgErr);

			//throw new BindingException(MsgErr);
		}
		
		String encodedPassword = passwordEncoder.encode(utente.getPassword());
		utente.setPassword(encodedPassword);
		utentiService.save(utente);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(utente.getId()).toUri();
		
		return ResponseEntity.created(location).build();
	}

	// ------------------- ELIMINAZIONE UTENTE ------------------------------------
	@DeleteMapping(value = "/elimina/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable("id") String userId)
	{
		logger.info("Eliminiamo l'utente con id " + userId);

		HttpHeaders headers = new HttpHeaders();
		ObjectMapper mapper = new ObjectMapper();

		headers.setContentType(MediaType.APPLICATION_JSON);

		ObjectNode responseNode = mapper.createObjectNode();

		Utenti utente = utentiService.getUtente(userId);

		if (utente == null)
		{
			String MsgErr = String.format("Utente %s non presente in anagrafica! ",userId);
			
			logger.warn(MsgErr);
			
			//throw new NotFoundException(MsgErr);
		}

		utentiService.delete(utente);

		responseNode.put("code", HttpStatus.OK.toString());
		responseNode.put("message", "Eliminazione Utente " + userId + " Eseguita Con Successo");

		return new ResponseEntity<>(responseNode, headers, HttpStatus.OK);
	}
}
