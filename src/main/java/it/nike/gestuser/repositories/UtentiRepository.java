package it.nike.gestuser.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import it.nike.gestuser.models.Utenti;

@Repository
public interface UtentiRepository extends MongoRepository<Utenti, String> {

    public Utenti findByUserId(String UserId);

}