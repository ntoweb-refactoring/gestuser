package it.nike.gestuser.services;

import java.util.List;


import it.nike.gestuser.models.Utenti;

public interface UtentiService {
    
    public List<Utenti> getAll();
	
	public Utenti getUtente(String userId);
	
	public void save(Utenti utente);
	
	public void delete(Utenti utente);
}