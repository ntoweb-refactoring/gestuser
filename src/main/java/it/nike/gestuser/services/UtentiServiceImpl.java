package it.nike.gestuser.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.nike.gestuser.models.Utenti;
import it.nike.gestuser.repositories.UtentiRepository;

@Service
@Transactional(readOnly = true)
public class UtentiServiceImpl implements UtentiService {

    @Autowired
    private UtentiRepository utentiRepository;
    
    
    @Override
    public List<Utenti> getAll() {
        return utentiRepository.findAll();
    }

    @Override
    public Utenti getUtente(String userId) {
        return utentiRepository.findByUserId(userId);
    }

    @Override
    public void save(Utenti utente) {
        utentiRepository.save(utente);
    }

    @Override
    public void delete(Utenti utente) {
        utentiRepository.delete(utente);
    }
    
}